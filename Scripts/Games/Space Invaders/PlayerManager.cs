﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace SpaceInvaders
{
    public class PlayerManager : MonoBehaviour
    {
        public float move_speed;
        public float shell_speed;

        [Space] // Ограничение передвижения игрока
        public float minX;
        public float maxX;
        public float minY;
        public float maxY;

        [Space]
        public EnemySpawner enemy_spawner;
        public Animator flash_animator; // Белая вспышка при проигрыше
        public Material white_flash;
        public GameObject shell_prefab;
        public GameObject[] hearts;

        public delegate void PlayerSounds(int code);
        public static event PlayerSounds OnPlayerPlaySound;

        private BoxCollider2D _collider;
        private ShellManager shell_manager;
        private Rigidbody2D _rigidbody;
        private GameObject created_prefab;
        private Transform fire_point;
        private Image image;
        private Vector3 movement;
        private Vector2 current_position;
        private Vector2 start_position;

        private float invincible_time = 0.15f; // Длительность неуязвимости
        private int health = 3;
        private bool isDead, isInvincible;

        private void Awake()
        {
            _collider = GetComponent<BoxCollider2D>();
            _rigidbody = GetComponent<Rigidbody2D>();
            image = GetComponent<Image>();
            fire_point = transform.GetChild(0);
            start_position = transform.position;

            Application.targetFrameRate = 60; // DEBUG
        }

        private void Update()
        {
            if (!isDead)
            {
                if (Input.GetMouseButtonDown(0)) Shoot();

                Movement();
            }
        }

        private void Movement()
        {
            movement.x = Input.GetAxis("Horizontal");
            movement.y = Input.GetAxis("Vertical");

            current_position = transform.localPosition + (movement * move_speed * 100 * Time.deltaTime);

            if (current_position.x < minX) current_position.x = minX;
            if (current_position.x > maxX) current_position.x = maxX;
            if (current_position.y < minY) current_position.y = minY;
            if (current_position.y > maxY) current_position.y = maxY;

            transform.localPosition = current_position;
        }

        private void Shoot()
        {
            created_prefab = Instantiate(shell_prefab, fire_point.position, Quaternion.identity, transform.parent);
            shell_manager = created_prefab.GetComponent<ShellManager>();
            shell_manager.IsAlly = true;
            shell_manager.Speed = shell_speed;

            _rigidbody.AddForce(Vector2.left * 10, ForceMode2D.Impulse);
            OnPlayerPlaySound(1); // Звук выстрела (code 1)
        }

        // Наносим урон игроку
        public void DoDamage()
        {
            if (!isDead && !isInvincible)
            {
                health--;
                hearts[health].SetActive(false);
                OnPlayerPlaySound(2); // Звук урона (code 2)
                CheckDeath();
                
                StartCoroutine(InvincibleTimer());
            }
        }

        private void CheckDeath()
        {
            if (health <= 0)
            {
                isDead = true;
                _collider.enabled = false;
                image.enabled = false;
                enemy_spawner.isOn = false;
                OnPlayerPlaySound(3); // Звук проигрыша (code 3)
                flash_animator.SetTrigger("Lose");
            }
        }

        // Таймер неуязвимости после получения урона
        private IEnumerator InvincibleTimer()
        {
            isInvincible = true;
            image.material = white_flash;

            yield return new WaitForSeconds(invincible_time);

            isInvincible = false;
            image.material = null;
        }

        private void OnEnable()
        {
            transform.position = start_position;
            _collider.enabled = true;
            image.enabled = true;
            isDead = false;
            health = 3;

            for (int i = 0; i < 3; i++)
            {
                hearts[i].SetActive(true);
            }
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            if (!isDead && other.GetComponent<EnemyManager>())
            {
                DoDamage();
                Destroy(other.gameObject);
            }
        }
    }
}