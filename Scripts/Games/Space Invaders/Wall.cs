﻿using UnityEngine;

namespace SpaceInvaders
{
    public class Wall : MonoBehaviour
    {
        public PlayerManager player;

        private void OnTriggerEnter2D(Collider2D other)
        {
            if (other.GetComponent<EnemyManager>())
            {
                player.DoDamage();
                Destroy(other.gameObject);
            }
        }
    }
}
