﻿using UnityEngine;

namespace SpaceInvaders
{
    public class ShellManager : MonoBehaviour
    {
        public float Speed { get; set; }
        public bool IsAlly { get; set; } // Союзный ли снаряд (true - да)

        private float newX;
        private int direction = 10; // Направление движения

        private void Start()
        {
            if (!IsAlly) direction = -10;

            Destroy(gameObject, 3);
        }

        private void Update()
        {
            newX = Mathf.MoveTowards(transform.localPosition.x, transform.localPosition.x + direction, Speed + 1 * Time.deltaTime);
            transform.localPosition = new Vector2(newX, transform.localPosition.y);
        }

        private void OnDisable()
        {
            Destroy(gameObject);
        }
    }
}
