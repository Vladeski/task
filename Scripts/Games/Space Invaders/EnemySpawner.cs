﻿using System.Collections;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    public Transform[] spawn_points;
    public GameObject[] enemy_prefabs;

    [Space]
    public float min_cd;
    public float max_cd;

    [HideInInspector]
    public bool isOn = true;

    private IEnumerator SpawnTick()
    {
        yield return new WaitForSeconds(Random.Range(min_cd, max_cd));

        if (isOn)
        {
            Instantiate(enemy_prefabs[Random.Range(0, 6)],
                spawn_points[Random.Range(0, 5)].position, Quaternion.identity, transform.parent);
        }

        StartCoroutine(SpawnTick());
    }

    private void OnDisable()
    {
        isOn = false;

        StopCoroutine(SpawnTick());
    }

    private void OnEnable()
    {
        isOn = true;

        StartCoroutine(SpawnTick());
    }
}
