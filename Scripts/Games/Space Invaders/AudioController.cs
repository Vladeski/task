﻿using System.Collections;
using UnityEngine;

namespace SpaceInvaders
{
    public class AudioController : MonoBehaviour
    {
        public AudioClip shot;
        public AudioClip hit;
        public AudioClip lose;

        private AudioSource _audio;

        private const int maxShots = 6;
        private const int maxHits = 5;
        private int current_shots;
        private int current_hits;

        private void Start()
        {
            _audio = GetComponent<AudioSource>();

            PlayerManager.OnPlayerPlaySound += PlayerSounds;
            EnemyManager.OnEnemyHitted += PlayHitSound;
        }

        private void PlayerSounds(int code)
        {
            switch (code)
            {
                case 1: PlayShotSound(); break;
                case 2: PlayHitSound(); break;
                case 3: PlayLoseSound(); break;
            }
        }

        private void PlayShotSound()
        {
            if (current_shots < maxShots)
            {
                current_shots++;
                _audio.pitch = Random.Range(0.9f, 1.15f);
                _audio.volume = 0.5f;
                _audio.PlayOneShot(shot);

                StartCoroutine(SoundCD(0));
            }
        }

        private void PlayHitSound()
        {
            if (current_hits < maxHits)
            {
                current_hits++;
                _audio.pitch = Random.Range(0.9f, 1.15f);
                _audio.volume = 0.3f;
                _audio.PlayOneShot(hit);

                StartCoroutine(SoundCD(1));
            }
        }

        private void PlayLoseSound()
        {
            _audio.pitch = 1;
            _audio.volume = 0.8f;
            _audio.PlayOneShot(lose);
        }

        private IEnumerator SoundCD(int code)
        {
            yield return new WaitForSeconds(0.25f);

            switch (code)
            {
                case 0: current_shots--; break;
                case 1: current_hits--; break;
            }
        }

        private void OnDisable()
        {
            current_hits = 0;
            current_shots = 0;
        }
    }
}
