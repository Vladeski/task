﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace SpaceInvaders
{
    public class EnemyManager : MonoBehaviour
    {
        public Material white_flash;
        public float move_speed;

        public delegate void EnemyHitted();
        public static event EnemyHitted OnEnemyHitted;

        private Rigidbody2D _rigidbody;
        private Image image;

        private float newX;
        private int health;
        private bool isAnimating;

        private void Awake()
        {
            _rigidbody = GetComponent<Rigidbody2D>();
            image = GetComponent<Image>();
            health = Random.Range(3, 5);
        }

        private void Update()
        {
            newX = Mathf.MoveTowards(transform.localPosition.x, transform.localPosition.x - 10, move_speed + 1 * Time.deltaTime);
            transform.localPosition = new Vector2(newX, transform.localPosition.y);
        }

        private void DoDamage()
        {
            health--;
            _rigidbody.AddForce(Vector2.right * 20, ForceMode2D.Impulse);
            OnEnemyHitted(); // Звук получения урона

            if (!isAnimating) StartCoroutine(HitTimer());
            if (health <= 0) Death();
        }

        private void Death()
        {
            Destroy(gameObject);
        }

        private IEnumerator HitTimer()
        {
            image.material = white_flash;
            isAnimating = true;

            yield return new WaitForSeconds(0.1f);

            image.material = null;
            isAnimating = false;
        }

        private void OnDisable()
        {
            Destroy(gameObject);
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            if (other.CompareTag("Shell"))
            {
                DoDamage();
                Destroy(other.gameObject);
            }
        }
    }
}
