﻿using UnityEngine;
using UnityEngine.UI;

public class GalleryButtons : MonoBehaviour
{
    public delegate void GalleryButtonPressed(Vector2 position, Vector2 local_scale, Vector2 size, Sprite sprite);
    public static event GalleryButtonPressed OnButtonPressed;

    private Sprite sprite;
    private Vector2 size;

    private void Awake()
    {
        GetComponent<Button>().onClick.AddListener(OnClick);
        sprite = GetComponent<Image>().sprite;
        size = GetComponent<RectTransform>().sizeDelta;
    }

    // Добавляем событие при нажатии на кнопку
    public void OnClick()
    {
        OnButtonPressed(transform.position, transform.localScale, size, sprite);
    }
}
