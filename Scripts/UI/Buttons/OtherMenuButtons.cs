﻿using UnityEngine;
using UnityEngine.UI;

public class OtherMenuButtons : MonoBehaviour
{
    public bool turnOn; // false - выключаем телефон

    public delegate void MenuButtonPressed(bool turnOn);
    public static event MenuButtonPressed OnButtonPressed;

    private void Awake()
    {
        GetComponent<Button>().onClick.AddListener(OnClick);
    }

    // Добавляем событие при нажатии на кнопку
    public void OnClick()
    {
        OnButtonPressed(turnOn);
    }
}
