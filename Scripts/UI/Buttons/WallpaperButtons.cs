﻿using UnityEngine;
using UnityEngine.UI;

public class WallpaperButtons : MonoBehaviour
{
    public delegate void MenuButtonPressed(Sprite sprite);
    public static event MenuButtonPressed OnButtonPressed;

    private Sprite sprite;

    private void Awake()
    {
        GetComponent<Button>().onClick.AddListener(OnClick);

        sprite = GetComponent<Image>().sprite;
    }

    // Передаём текущий спрайт кнопки
    public void OnClick()
    {
        OnButtonPressed(sprite);
    }
}
