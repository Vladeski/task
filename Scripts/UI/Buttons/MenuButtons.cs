﻿using UnityEngine;
using UnityEngine.UI;

public class MenuButtons : MonoBehaviour
{
    public GameObject current_menu;

    public delegate void MenuButtonPressed(GameObject menu);
    public static event MenuButtonPressed OnButtonPressed;

    private void Awake()
    {
        GetComponent<Button>().onClick.AddListener(OnClick);
    }

    // Добавляем событие при нажатии на кнопку
    public void OnClick()
    {
        OnButtonPressed(current_menu);
    }
}
