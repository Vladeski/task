﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

/*  
 *      Player Prefs:
 *          
 *      1) Exp - опыт
 *      2) Level - уровень
 *  
 */ 
public class SkillsManager : MonoBehaviour
{
    public Slider slider;
    public Text text_level;

    private float animation_time = 1f; // Длительность анимации
    private int new_exp, total_exp, lvl;

    private void Awake()
    {
        new_exp = PlayerPrefs.GetInt("Exp", 0);
        total_exp = PlayerPrefs.GetInt("TotalExp");
        lvl = PlayerPrefs.GetInt("Level", 1);
        text_level.text = lvl.ToString();
        slider.value = new_exp;
    }

    // Добавляем опыт и запускаем анимацию
    private void AddExp()
    {
        new_exp += 20;
        total_exp += 20;

        StartCoroutine(SliderAnimation());
    }

    // Проверяем доступен ли новый уровень
    private void CheckNewLvl()
    {
        if (new_exp >= 100)
        {
            lvl++;
            new_exp = 0;
            text_level.text = lvl.ToString();
        }

        slider.value = new_exp;

        SaveData();
    }

    // Сохраняем данные
    private void SaveData()
    {
        PlayerPrefs.SetInt("Exp", new_exp);
        PlayerPrefs.SetInt("Level", lvl);
        PlayerPrefs.SetInt("TotalExp", total_exp);
    }

    // Анимируем слайдер
    private IEnumerator SliderAnimation()
    {
        yield return new WaitForSeconds(0.3f); // Небольшая задержка перед началом

        float current_exp = slider.value;
        float time = 0;

        while (time < animation_time)
        {
            slider.value = Mathf.Lerp(current_exp, new_exp, time / animation_time);
            time += Time.deltaTime;

            yield return null;
        }

        slider.value = new_exp;

        CheckNewLvl();
    }

    private void OnEnable()
    {
        AddExp();
    }

    // На случай если вышли из меню до конца анимации
    private void OnDisable()
    {
        StopCoroutine(SliderAnimation());

        CheckNewLvl();
    }
}
