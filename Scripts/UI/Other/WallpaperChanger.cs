﻿using UnityEngine;
using UnityEngine.UI;

public class WallpaperChanger : MonoBehaviour
{
    private Image image;

    private void Start()
    {
        image = GetComponent<Image>();

        WallpaperButtons.OnButtonPressed += ChangeWallpaper; // Кнопки телефона
    }

    private void ChangeWallpaper(Sprite sprite)
    {
        image.sprite = sprite;
    }
}
