﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class GalleryManager : MonoBehaviour
{
    public Transform container;
    public GameObject image_prefab;

    private CanvasGroup container_canvas;
    private GameObject created_prefab;
    private Vector2 startScale;
    private Vector2 startPosition;

    private float animation_time = 0.2f;

    private bool isAnimating; // Находимся ли в состоянии анимации
    private bool isFullscreen; // На полный ли экран фото

    private void Start()
    {
        container_canvas = container.GetComponent<CanvasGroup>();

        GalleryButtons.OnButtonPressed += CreateImage; // Кнопки телефона
    }

    private void CreateImage(Vector2 position, Vector2 local_scale, Vector2 size, Sprite sprite)
    {
        if (!isAnimating)
        {
            // Если будем "увеличивать" картинку
            if (!isFullscreen)
            {
                created_prefab = Instantiate(image_prefab, position, Quaternion.identity, container);
                created_prefab.transform.localScale = local_scale;
                created_prefab.GetComponent<Image>().sprite = sprite;
                created_prefab.GetComponent<RectTransform>().sizeDelta = size;

                startScale = local_scale;
                startPosition = position;

                StartCoroutine(ZoomOutPhoto());
            }
            else
            {
                StartCoroutine(ZoomInPhoto());
            }
        }
    }

    // Делает фото на весь экран
    private IEnumerator ZoomOutPhoto()
    {
        container.gameObject.SetActive(true);
        isAnimating = true;
        isFullscreen = true;

        Vector2 startScale = created_prefab.transform.localScale;
        Vector2 startPosition = created_prefab.transform.localPosition;

        float time = 0;

        while (time < animation_time)
        {
            created_prefab.transform.localScale = Vector3.Lerp(startScale, Vector2.one, time / animation_time);
            created_prefab.transform.localPosition = Vector3.Lerp(startPosition, Vector2.zero, time / animation_time);
            container_canvas.alpha = time / animation_time;
            time += Time.deltaTime;

            yield return null;
        }

        created_prefab.transform.localScale = Vector2.one;
        created_prefab.transform.localPosition = Vector2.zero;
        container_canvas.alpha = 1;
        isAnimating = false;
    }

    // Уменьшаем фото
    private IEnumerator ZoomInPhoto()
    {
        isAnimating = true;
        isFullscreen = false;

        Vector2 currentScale = created_prefab.transform.localScale;
        Vector2 currentPosition = created_prefab.transform.position;

        float time = 0;

        while (time < animation_time)
        {
            created_prefab.transform.localScale = Vector3.Lerp(currentScale, startScale, time / animation_time);
            created_prefab.transform.position = Vector3.Lerp(currentPosition, startPosition, time / animation_time);
            container_canvas.alpha = 1 - (time / animation_time);
            time += Time.deltaTime;

            yield return null;
        }

        created_prefab.transform.localScale = startScale;
        created_prefab.transform.position = startPosition;
        container_canvas.alpha = 0;
        container.gameObject.SetActive(false);
        isAnimating = false;
        Destroy(created_prefab.gameObject);
    }
}
