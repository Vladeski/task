﻿using System.Collections;
using UnityEngine;

public class PhoneManager : MonoBehaviour
{
    public GameObject phone_canvas; // Телефон
    public GameObject main_menu;

    public GameObject current_menu { get; set; }

    private Animator phone_animator;
    private CanvasGroup canvas_group;

    private int tab_clicks;
    private bool isEnabled;

    private void Awake()
    {
        phone_animator = GetComponent<Animator>();
        tab_clicks = PlayerPrefs.GetInt("Tabs");
        current_menu = main_menu;

        Application.targetFrameRate = 60;
    }

    private void Start()
    {
        MenuButtons.OnButtonPressed += OpenMenu; // Кнопки телефона
        OtherMenuButtons.OnButtonPressed += ChangePhoneState; // Кнопки правого меню (вкл/выкл телефон)
    }

    // DEBUG
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Delete)) PlayerPrefs.DeleteAll();
    }

    // Открываем меню телефона
    public void OpenMenu(GameObject menu)
    {
        tab_clicks++;

        // Кол-во нажатых вкладок
        PlayerPrefs.SetInt("Tabs", tab_clicks);

        // Если нажали кнопку "Назад" в "Главном меню"
        if (menu == main_menu && current_menu == main_menu)
        {
            ChangePhoneState(false); // Выключаем телефон
        }
        else
        {
            current_menu.SetActive(false);
            current_menu = menu;
            current_menu.SetActive(true);
            canvas_group = current_menu.GetComponent<CanvasGroup>();

            StartCoroutine(MenuFade());
        }
    }

    // Меняем состояние телефона
    private void ChangePhoneState(bool turnOn)
    {
        // Включаем
        if (turnOn)
        {
            if (!phone_canvas.activeSelf)
            {
                phone_animator.SetBool("isOn", true);
                isEnabled = true;

                PlayerPrefs.SetInt("Openings", PlayerPrefs.GetInt("Openings") + 1);
            }
        }
        // Выключаем
        else
        {
            if (phone_canvas.activeSelf)
            {
                phone_animator.SetBool("isOn", false);
                isEnabled = false;

                PlayerPrefs.SetInt("Closures", PlayerPrefs.GetInt("Closures") + 1);
            }
        }
    }

    // Обнуляем телефон
    private void ResetPhone()
    {
        current_menu.SetActive(false);
        current_menu = main_menu;
        current_menu.SetActive(true);
    }

    // Выключем "анимацию"
    public void DisableAnimation()
    {
        ResetPhone();

        if (isEnabled)
        {
            Debug.Log("Telephone was enabled.");
        }
        else
        {
            Debug.Log("Telephone was disabled.");
        }
    }

    // Анимация выцветания меню при открытии
    private IEnumerator MenuFade()
    {
        float time = 0;

        while (time < 0.25f)
        {
            canvas_group.alpha = Mathf.Lerp(0, 1, time / 0.25f);
            time += Time.deltaTime;

            yield return null;
        }

        canvas_group.alpha = 1;
    }
}
