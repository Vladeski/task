﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

/*
 *      Player Prefs:
 *      
 *      1) Openings - открытий телефона
 *      2) Closures - закрытий телефона
 *      3) Tabs - открытий вкладок
 *      4) TotalExp - общее кол-во заработанного опыта
 *      5) Level - текущий уровень
 * 
 */
public class ExtraManager : MonoBehaviour
{

    private Text text_stats;

    private string basic_text;
    private float type_time = 2f; // Время печати текста
    private float score;

    private void Awake()
    {
        text_stats = transform.GetChild(0).GetComponent<Text>();
        basic_text = name + ":  ";
    }

    // Берём данные для статистики
    private int GetScore()
    {
        text_stats.text = basic_text + "0";

        switch (name)
        {
            case "Phone openings": return PlayerPrefs.GetInt("Openings");
            case "Phone closures": return PlayerPrefs.GetInt("Closures");
            case "Tabs clicked": return PlayerPrefs.GetInt("Tabs");
            case "Exp earned": return PlayerPrefs.GetInt("TotalExp");
            case "Current level": return PlayerPrefs.GetInt("Level");

            default: return 0;
        }
    }

    // Анимация цифер в статистике
    private IEnumerator TextTyper()
    {
        yield return new WaitForSeconds(0.3f); // Небольшая задержка перед началом

        float start_num = score;
        float time = 0;

        while (time < type_time)
        {
            score = Mathf.Lerp(0, start_num, time / type_time);
            time += Time.deltaTime;
            text_stats.text = basic_text + ((int)score).ToString();

            yield return null;
        }

        text_stats.text = basic_text + ((int)start_num).ToString();
    }

    private void OnEnable()
    {
        StopCoroutine(TextTyper());

        score = GetScore();

        StartCoroutine(TextTyper());
    }
}
