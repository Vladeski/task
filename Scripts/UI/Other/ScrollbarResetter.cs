﻿using UnityEngine;
using UnityEngine.UI;

// Сбрасываем скроллбары при выключении меню
public class ScrollbarResetter : MonoBehaviour
{
    private Scrollbar scrollbar;

    private bool missScrollbar;

    private void Awake()
    {
        scrollbar = GetComponent<Scrollbar>();

        // Если нет скролл-бара
        if (scrollbar == null) missScrollbar = true;
    }

    private void OnEnable()
    {
        if (!missScrollbar) scrollbar.value = 1;
        else transform.localPosition = Vector2.zero;
    }
}
